# Run Dijkstra's shortest-path algorithm on this graph, 
# using 1 (the first vertex) as the source vertex, 
# and to compute the shortest-path distances between 1 and every other vertex of the graph. 
# If there is no path between a vertex v and vertex 1, 
# we'll define the shortest-path distance between 1 and v to be 1000000

# Input - The file contains an adjacency list representation of an 
# undirected weighted graph with 200 vertices labeled 1 to 200.  
# Each row consists of the node tuples that are adjacent to that 
# particular vertex along with the length of that edge. 
# For example, the 6th row has 6 as the first entry indicating 
# that this row corresponds to the vertex labeled 6. 
# The next entry of this row "141,8200" indicates that there is an 
# edge between vertex 6 and vertex 141 that has length 8200.  
# The rest of the pairs of this row indicate the other vertices 
# adjacent to vertex 6 and the lengths of the corresponding edges
num_vertices = 200

def find_min_to_node(node):
    # Check all the adjacent nodes
    for i in range(len(adj_node[node - 1])):
        adj_node1 = adj_node[node - 1][i]
        
        # If the adjacent node is explored, then 
        # compare the distance to find min distance
        # to the input node
        if (node_explored[adj_node1 - 1] == 1):
            adj_dist1 = adj_node_dist[node - 1][i]
            dist_from_node = adj_dist1 + min_dist[adj_node1 - 1]
            if (min_dist[node - 1] > dist_from_node):
                min_dist[node - 1] = dist_from_node
                min_path[node - 1] = min_path[adj_node1 - 1] + [adj_node1]
                #min_path[node - 1].append(adj_node1)

def update_dist_of_node(node):
    # Check all the adjacent nodes
    for i in range(len(adj_node[node - 1])):
        adj_node1 = adj_node[node - 1][i]
        if (node_explored[adj_node1 - 1] == 1):
            # Update the min distance for the node in the adj node list
            # If the distance is updated for any Adj node, then 
            # check the same for their own Adj nodes
            adj_dist1 = adj_node_dist[node - 1][i]
            if (min_dist[adj_node1 - 1] > min_dist[node - 1] + adj_dist1):
                min_dist[adj_node1 - 1] = min_dist[node - 1] + adj_dist1
                min_path[adj_node1 - 1] = min_path[node - 1] + [node]
                update_dist_of_node(adj_node1)

def dijkstras_algo(node):
    node_explored[node - 1] = 1
    find_min_to_node(node)
    update_dist_of_node(node)

file = open('dijkstraData.txt')
lines = file.readlines()
#adj_list = [[] for i in range(num_vertices)]
adj_node = [[] for i in range(num_vertices)]
adj_node_dist = [[] for i in range(num_vertices)]
for line in lines:
    a = line.split()
    for item in range(1, len(a)):
        #adj_list[int(a[0]) - 1].append(a[item])
        b = a[item].split(',')
        # Get all the adjacent nodes and distance 
        # separated from the txt file
        adj_node[int(a[0]) - 1].append(int(b[0]))
        adj_node_dist[int(a[0]) - 1].append(int(b[1]))

#print('Adjacency Node', adj_node)
#print('Adjacency Node Distance', adj_node_dist)
# Set the min distance for all the nodes
min_dist = [1000000000] * num_vertices
min_path = [[] for i in range(num_vertices)]
node_explored = [0] * num_vertices

# Start with the first node
node = 1
# Set the min distance of the source node to 0
min_dist[0] = 0

node_explored[0] = 1
count = 0
# Start from the second node, as first node is explored
for i in range(2, num_vertices + 1):
#while (count < num_vertices):
#    for i in adj_node[node - 1]:
    if (node_explored[i - 1] == 0):
        node = i
        #print('next node', node)
        dijkstras_algo(node)
        #break
    
    #dijkstras_algo(node)
    #count += 1

# print('min_dist', min_dist)
# print('node explored', node_explored)
# print('min_path', min_path)
# Assignment output
print (min_dist[7 - 1],
min_dist[37 - 1],
min_dist[59 - 1],
min_dist[82 - 1],
min_dist[99 - 1],
min_dist[115 - 1],
min_dist[133 - 1],
min_dist[165 - 1],
min_dist[188 - 1],
min_dist[197 - 1])
