# Code up and run the randomized contraction algorithm (Karger Algo) 
# for the min cut problem and use it on the above graph 
# to compute the min cut

# The file contains the adjacency list representation of a simple undirected graph. 
# There are 200 vertices labeled 1 to 200. The first column in the file represents 
# the vertex label, and the particular row (other entries except the first column) 
# tells all the vertices that the vertex is adjacent to. So for example, the 6th row 
# looks like : "6	155	56	52	120	......". This just means that the vertex with 
# label 6 is adjacent to (i.e., shares an edge with) the vertices with labels 155,56,52,120,....etc

import random

# import numpy
min_cut_count = 0
def min_cut(r1, r2, matrix):

    # Find the actual vertex with the indexes
    r1_idx = matrix[r1][0]
    r2_idx = matrix[r2][0]
    #print('Selecting', r1_idx, 'and', r2_idx)
    # remove self loop entries, meaning remove first index from the second index's edges
    # remove() method only removes the first matching element thats why while loop
    while (r1_idx in matrix[r2]):
        matrix[r2].remove(r1_idx)
    # matrix[r2].remove(matrix[r1][0])
    # remove self loop entries, meaning remove second index from the first index's edges
    # remove() method only removes the first matching element thats why while loop
    while (r2_idx in matrix[r1]):
        matrix[r1].remove(r2_idx)
    # replace all the second index with first in the entire matrix
    # contracting the 2 vertex into 1
    i = 0
    for rows in matrix:
        matrix[i] = [r1_idx if j==r2_idx else j for j in matrix[i]]
        i += 1
    # remove index of the second row before merging
    matrix[r2].pop(0)
    # merge the 2 rows, contract
    matrix[r1] = matrix[r1] + matrix[r2]
    # remove the second vertex 
    matrix.pop(r2)
    i = 0
    # remove duplicates in the new matrix - No need in this algorithm
    # for rows in matrix:
    #     res = []
    #     [res.append(x) for x in matrix[i] if x not in res]
    #     matrix[i] = res
    #     # set changes the order in the list
    #    #matrix[i] = list(set(rows))
    #     i += 1
    # print(matrix)
    if(len(matrix) > 2):
        # print(matrix)
        # Choose first vertex randomly
        row_idx1 = random.randint(0, len(matrix) - 1)
        i = 0
        # Choose second vertex adjacaent to the first one
        # Use the first vertex adjacent in the vertex chosen, then find 
        # the index (i) of that vertex
        for row in matrix:
            if (row[0] != matrix[row_idx1][1]):
                i += 1
            else:
                break
        row_idx2 = i
        #print('Selecting', matrix[row_idx1][0], 'and', matrix[row_idx2][0])

        num_cut_edges = min_cut(row_idx1, row_idx2, matrix)
        return num_cut_edges
    else:
        # print('Final one', matrix)
        # The min number of edges crossed or cut is the number of edges 
        # adjacent to the vertex (meaning len - 1)
        # -1 to exclude the vertex itself in the matrix
        return (len(matrix[0]) - 1)

## main ##
filename = 'kargerMinCut.txt'
file = open(filename)

# Read the lines separately in a list
lines = file.readlines()
#print(matrix)
#print('Give Graph matrix is ', matrix)

num_trial = 0
min_num_cut = len(lines)
# Number of trials approx double the size of the vertexes
while (num_trial < (2 * len(lines))):
    in_matrix = []
    for line in lines:
        # Separate each character in lines
        a = line.split()
        # Convert each character in int
        b =  map(int, a)
        # Add to the 2d array
        in_matrix.append(list(b))
    if(len(in_matrix) > 2):
        # Choose random rows (nodes to contract)
        row_idx1 = 0
        row_idx2 = 0
        # Choose first vertex randomly
        row_idx1 = random.randint(0, len(in_matrix) - 1)
        i = 0
        # print('Looking for index with vertex', matrix[row_idx1][1])
        # Choose second vertex adjacaent to the first one
        # Use the first vertex adjacent in the vertex chosen, then find 
        # the index (i) of that vertex
        for row in in_matrix:
            if (row[0] != in_matrix[row_idx1][1]):
                i += 1
            else:
                break
        row_idx2 = i
        #print('Selecting', matrix[row_idx1][0], 'and', matrix[row_idx2][0])
        # Call the function to cut the graph using the index of the matrix
        num_cut_edges = min_cut(row_idx1, row_idx2, in_matrix)

    print('Trial', num_trial, 'Min num edges cut', num_cut_edges)
    if(min_num_cut > num_cut_edges):
        min_num_cut = num_cut_edges
        print('\tMinimum number of edges cut change to', min_num_cut)
    num_trial += 1

print ('Final Minimum number of edges cut', min_num_cut)
