# Compute the total number of comparisons used to sort
# the given input file by QuickSort
# You should not count comparisons one-by-one.  
# Rather, when there is a recursive call on a subarray of length mmm, you should simply add m−1 to your running total of comparisons.  
# (This is because the pivot element is compared to each of the other m−1 elements in the subarray in this recursive call.)
# Use the first element of the array as the pivot element.
compare_count = 0
def partition(arr):
    global compare_count
    if (len(arr) == 1):
        return arr
    else:
        # Choosing first element as the pivot
        pivot_idx = 0
        # Increment comparision count by length of array - 1
        # Since pivot will be compared to n - 1 elements in the array
        compare_count += len(arr) - 1
        # Parition the array around the pivot
        left_arr = []
        right_arr = []
        i = 1
        # Exclude the first element in the comparision as it is the pivot itself
        for j in range(i, len(arr)):
            if (arr[pivot_idx] > arr[j]):
                # swap the elements
                arr[i], arr[j] = arr[j], arr[i]
                i += 1
        # swap the pivot to its rightful index
        if (i > 1):
            arr[pivot_idx], arr[i - 1] = arr[i - 1], arr[pivot_idx]
            left_arr = arr[: i - 1]
            pivot_idx = i - 1
        if (i <= j):
            right_arr = arr[i:]

        ## Recursive calls for both left and right array
        # Skip the array if its empty
        sorted_arr = []
        if (len(left_arr) != 0):
            left_arr = partition(left_arr)
            sorted_arr = left_arr
        sorted_arr.append(arr[pivot_idx])
        # Skip the array if its empty
        if (len(right_arr) != 0):
            right_arr = partition(right_arr)    
            sorted_arr = sorted_arr + right_arr

        return sorted_arr

filename = 'QuickSort.txt'

file = open(filename, 'r')

lines = file.readlines()
# File is in the str type, convert first into int type
intArray = [int(numeric_string) for numeric_string in lines]

sorted_array = partition(intArray)
#print(sorted_array)
print("Total Number of comparision is", compare_count)
