# Implement one or more of the integer multiplication algorithms
# The Program is an implementation of the Karatsuba's Algorithm
# using the recusion method (using integer values for the operation)
import sys
x = 3141592653589793238462643383279502884197169399375105820974944592
y = 2718281828459045235360287471352662497757247093699959574966967627
#x.y = 8539734222673567065463550869546574495034888535765114961879601127067743044893204848617875072216249073013374895871952806582723184

# Takes 2 integer numbers as input and multiply them 
# using Karatsuba's Algorithm
# x = (10^n/2)a + b and y = (10^n/2)c + d
# x.y = ((10^n/2)a + b) ((10^n/2)c + d)
#     = (10^n)ac + (10^n/2)(ad + bc) + bd

def multiply(x, y):
    x_digits = len(str(x))
    y_digits = len(str(y))

    if (x_digits == 1 and y_digits == 1):
        return x*y
    else:
        # check if x_digits and y_digits are equal
        # if not then pad 0 in the beginning
        #if (x_digits != y_digits):
        max_digits = max(x_digits, y_digits)
        if (max_digits % 2 != 0):
            max_digits += 1;
        # Break the numbers into half to do the karatsubas algo
        x1 = int(x//(10 ** (max_digits//2)))   # first n/2 digits in the number
        x2 = int(x%(10 ** (max_digits//2)))    # second n/2 digits in the number
        y1 = int(y//(10 ** (max_digits//2)))   # first n/2 digits in the number
        y2 = int(y%(10 ** (max_digits//2)))    # second n/2 digits in the number
        product = (10 ** max_digits) * multiply (x1, y1) + \
            (10 ** (max_digits//2)) * (multiply (x1, y2) + multiply (x2, y1)) + \
            multiply(x2, y2)
        return product

# print(sys.maxsize)
product = multiply(x, y)
print(product)
