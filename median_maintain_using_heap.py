# The goal of this problem is to implement the "Median Maintenance"
# algorithm (covered in the Week 3 lecture on heap applications).  
# The text file contains a list of the integers from 1 to 10000 in unsorted order; 
# you should treat this as a stream of numbers, arriving one by one.
# Letting Xi​ denote the ith number of the file, 
# the kth median mk​ is defined as the median of the numbers X1,…,Xk.
# (So, if k is odd, then mk​ is ((k+1)/2)th 
# smallest number among X1,…,Xk​; if k is even, 
# then mk is the (k/2)th smallest number among X1,…,Xk)

# In the box below you should type the sum of these 10000 medians, 
# modulo 10000 (i.e., only the last 4 digits).  That is, you should 
# compute (m1+m2+m3+⋯+m10000) mod 10000.

# Insert a new number in the bottom and 
# check the heap_low property till the root
# From bottom to top
def heap_low_insert(heap, new_num):
    heap.append(new_num)
    # Property of heap_low - The parent's key should
    # be higher than children's keys

    # set the index to the last one, since that is where we
    # added the new element
    num_index = len(heap) - 1

    while True:
        # If the index is root or has reached root, then stop
        if (num_index == 0):
            return
        # If the index is not root, find the parent index
        if (num_index % 2 == 0):
            parent_index = num_index//2 - 1
        else:
            parent_index = num_index//2
        # Find the child indexes
        child1_index = 2 * parent_index + 1
        child2_index = 2 * parent_index + 2
        # If the len of the heap, is greater then highes child index
        # that means, both childs are present else only 1 child or no child
        if ((len(heap) - 1) >= child2_index):
            # find the max of both child to compare it with parent
            if (heap[child1_index] > heap[child2_index]):
                max_index = child1_index
            else:
                max_index = child2_index
        elif ((len(heap) - 1) >= child1_index):
            max_index = child1_index
        else:
            break

        # If the child's key is greater than parent, replace it
        # set the new index to previous parent to go uptill root
        if (heap[parent_index] < heap[max_index]):
            heap[parent_index], heap[max_index] = heap[max_index], heap[parent_index]
            num_index = parent_index
        else:
            break

# Insert a new number in the bottom and 
# check the heap_high property till the root
# From bottom to top
def heap_high_insert(heap, new_num):
    heap.append(new_num)
    # Property of heap_high - The parent's key should
    # be lower than children's keys

    # set the index to the last one, since that is where we
    # added the new element
    num_index = len(heap) - 1

    while True:
        # If the index is root or has reached root, then stop
        if (num_index == 0):
            return
        # If the index is not root, find the parent index
        if (num_index % 2 == 0):
            parent_index = num_index//2 - 1
        else:
            parent_index = num_index//2
        # Find the child indexes
        child1_index = 2 * parent_index + 1
        child2_index = 2 * parent_index + 2
        # If the len of the heap, is greater then highes child index
        # that means, both childs are present else only 1 child or no child
        if ((len(heap) - 1) >= child2_index):
            # find the min of both child to compare it with parent
            if (heap[child1_index] < heap[child2_index]):
                min_index = child1_index
            else:
                min_index = child2_index
        elif ((len(heap) - 1) >= child1_index):
            min_index = child1_index
        else:
            break

        # If the child's key is lower than parent, replace it
        # set the new index to previous parent to go uptill root
        if (heap[parent_index] > heap[min_index]):
            heap[parent_index], heap[min_index] = heap[min_index], heap[parent_index]
            num_index = parent_index
        else:
            break

def extract_max(heap):
    parent_index = 0

    # Replace root with the bottom element
    # Run down to satisfy the extract max property
    # key of parent is always higher than children
    heap[parent_index] = heap[-1]
    # Remove the bottom element
    heap.pop()
    while True:
        child1_index = 2 * parent_index + 1
        child2_index = 2 * parent_index + 2
        if ((len(heap) - 1) >= child2_index):
            if (heap[child1_index] > heap[child2_index]):
                max_index = child1_index
            else:
                max_index = child2_index
        elif ((len(heap) - 1) >= child1_index):
            max_index = child1_index
        else:
            break   

        # If the child's key is greater than parent, replace it
        # set the new index to previous parent to go uptill root
        if (heap[parent_index] < heap[max_index]):
            heap[parent_index], heap[max_index] = heap[max_index], heap[parent_index]
            parent_index = max_index
        else:
            break
        
def extract_min(heap):
    parent_index = 0

    # Replace root with the bottom element
    # Run down to satisfy the extract min property
    # key of parent is always lower than children
    heap[parent_index] = heap[-1]
    # Remove the bottom element
    heap.pop()
    while True:
        child1_index = 2 * parent_index + 1
        child2_index = 2 * parent_index + 2
        if ((len(heap) - 1) >= child2_index):
            if (heap[child1_index] < heap[child2_index]):
                min_index = child1_index
            else:
                min_index = child2_index
        elif ((len(heap) - 1) >= child1_index):
            min_index = child1_index
        else:
            break   

        # If the child's key is lower than parent, replace it
        # set the new index to previous parent to go uptill root
        if (heap[parent_index] > heap[min_index]):
            heap[parent_index], heap[min_index] = heap[min_index], heap[parent_index]
            parent_index = min_index
        else:
            break
        
filename = 'median.txt'
file = open(filename)
x = []
heap_low = []
heap_high = []
# Read the lines separately in a list
lines = file.readlines()
for number in lines:
    # Separate each character in lines
    x.append(int(number))

# print(x)

median = []

heap_high.append(x[0])
median.append(x[0])
output = x[0]
for number in x[1:]:
    # If the number is higher than heap_high root,
    # then add the number in the heap_high because
    # heap_high contains bigger half of the numbers
    if (number > heap_high[0]):
        heap_high_insert(heap_high, number)
        # print('Adding in high', number)
    # If the number is smaller then root of heap_high but bigger
    # than the root of heap_low, then also add it in the heap_high
    # because of the same reason as above EXCEPT the second element
    # which goes in the heap_low
    elif (number != x[1]):
        if (number > heap_low[0]):
            heap_high_insert(heap_high, number)
            # print('Adding in high', number)
        else:
            heap_low_insert(heap_low, number)
            # print('Adding in low', number)
    else:
        # if the number is less than root of heap_low
        # then add it to the heap_low
        heap_low_insert(heap_low, number)
        # print('Adding in low', number)
    
    # Check if the heap_low and heap_high are balanced
    # i.e. Check if they have n/2 elemnets
    # if not insert extra numbers from heap_low to heap_high
    # or vice versa
    if (len(heap_low) >= len(heap_high) + 2):
        # root of heap_low -> bottom of heap_high
        heap_high_insert(heap_high, heap_low[0])
        extract_max(heap_low)
    elif (len(heap_high) >= len(heap_low) + 2 ):
        # root of heap_high -> bottom of heap_low
        heap_low_insert(heap_low, heap_high[0])
        extract_min(heap_high)

    # print('heap_low', heap_low)
    # print('heap_high', heap_high)
    total_length = len(heap_low) + len(heap_high)

    # Find if the element added makes total
    # count even or odd
    # It gives us the numbe r of order statistics to look for
    if (total_length % 2 == 0):
        median_order = total_length//2
    else:
        median_order = (total_length + 1)//2
    # If the order statistics resides in the heap_low
    # then it will be top of the heap_low
    # if not, then it is in the heap_high. 
    # then decrement the order by len of heap_low to get median
    if (len(heap_low) >= median_order):
        median.append(heap_low[0])
    else:
        median.append(heap_high[median_order - len(heap_low) - 1])

    # Add the median to the output for the assignment
    output += median[-1]

# Module output of assignment
# Addition of all the medians modulo 10000
print (output % 10000) 
