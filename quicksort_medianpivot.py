# Compute the number of comparisons (as in Problem 1), 
# using the "median-of-three" pivot rule.

# SUBTLE POINT: A careful analysis would keep track of the comparisons made 
# in identifying the median of the three candidate elements.  
# You should NOT do this.  That is, as in the previous two problems, you 
# should simply add m−1 to your running total of comparisons every 
# time you recurse on a subarray with length m

import math
compare_count = 0
def partition(arr):
    global compare_count
    if (len(arr) == 1):
        return arr
    else:
        #print(arr)
        len_arr = len(arr)
        sorted_arr = []
        # Choosing median of the first, last and middle element as the pivot
        # If the array has odd length it should be clear what the "middle" element is; 
        # for an array with even length 2k, use the kth element as the
        # "middle" element
        # pivot = arr[0, arr[len/2], arr[len(arr) - 1]]

        first = arr[0]
        middle = arr[math.ceil(len_arr/2) - 1]
        last = arr[len_arr - 1]
        if (first < middle):
            if (middle < last):
                pivot_index = math.ceil(len_arr/2) - 1
            else:
                if (first < last):
                    pivot_index = len(arr) - 1
                else:
                    pivot_index = 0
        else:
            if (first < last):
                pivot_index = 0
            else:
                if (middle < last):
                    pivot_index = len_arr - 1
                else:
                    pivot_index = math.ceil(len_arr/2) - 1

        # Swap the first and median elements
        arr[0], arr[pivot_index] = arr[pivot_index], arr[0]
        pivot_index = 0
        # Increment comparision count by length of array - 1
        # Since pivot will be compared to n - 1 elements in the array
        compare_count += len(arr) - 1
        #print(len(arr) - 1, "median", arr[0])
        # Parition the array around the pivot
        left_arr = []
        right_arr = []
        i = 1
        # Exclude the first element in the comparision as it is the pivot itself
        for j in range(i, len(arr)):
            if (arr[pivot_index] > arr[j]):
                # swap the elements
                arr[i], arr[j] = arr[j], arr[i]
                i += 1
        # swap the pivot to its rightful index
        if (i > 1):
            arr[pivot_index], arr[i - 1] = arr[i - 1], arr[pivot_index]
            left_arr = arr[: i - 1]
            pivot_index = i - 1
        if (i <= j):
            right_arr = arr[i:]

        ## Recursive calls for both left and right array
        # Skip the array if its empty
        sorted_arr = []
        if (len(left_arr) != 0):
            left_arr = partition(left_arr)
            sorted_arr = left_arr
        sorted_arr.append(arr[pivot_index])
        # Skip the array if its empty
        if (len(right_arr) != 0):
            right_arr = partition(right_arr)    
            sorted_arr = sorted_arr + right_arr

        return sorted_arr

filename = 'QuickSort.txt'

file = open(filename, 'r')

lines = file.readlines()
# File is in the str type, convert first into int type
intArray = [int(numeric_string) for numeric_string in lines]

sorted_array = partition(intArray)
#print(sorted_array)
print("Total Number of comparision is", compare_count)
