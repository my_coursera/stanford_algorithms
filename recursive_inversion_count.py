# The input file contains all of the 100,000 integers between 1 and 100,000
# (inclusive) in some order, with no integer repeated.

# Compute the number of inversions in the file given using 
# fast divide-and-conquer algorithm

# import sys
# sys.setrecursionlimit(10000)

count_inv = 0
def merge_sort(a, b):
    global count_inv
    if ((len(a) == len(b)) and (len(a) == 1)):
        if a < b:
            return a+b
        else:
            count_inv += 1
            return b+a
    else:
        # Divide the array into 2 parts
        if (len(a) != 1):
            a = merge_sort(a[:len(a)//2], a[len(a)//2:])
        if (len(b) != 1):
            b = merge_sort(b[:len(b)//2], b[len(b)//2:])
        i = j = k = 0
        c = []
        while k < len(a) + len(b):
            #Build array c with sorted entries
            if (a[i] < b[j]):
                c.append(a[i])
                i += 1
                if (i == len(a)):
                    c = c + b[j:len(b)]
                    break
            else:
                c.append(b[j])
                j += 1
                count_inv += len(a) - i
                if (j == len(b)):
                    c = c + a[i:len(a)]
                    break
            k += 1
        return c

filename = 'IntegerArray.txt'

file = open(filename, 'r')
#file = [16,2,4,3]
lines = file.readlines()
intArray = [int(numeric_string) for numeric_string in lines]
count = 0

# Strips the newline character
for num in intArray:
    count += 1

final = merge_sort(intArray[:len(intArray)//2], intArray[len(intArray)//2:])
print("Total number of inversion:", count_inv)
#print(final)
